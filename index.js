const inquirer = require('inquirer');
const fs = require('fs');
const { exec } = require('child_process');
const questions = [
    {
        name: 'name',
        type: 'input',
        message: 'What is the project name?',
        validate(value) {
            if (value.length > 0 && !fs.existsSync(__dirname + value)) {
                return true;
            } else {
                return 'Please enter a valid or non existing project name.'
            }
        }
    },
    {
        name: 'technologies',
        type: 'checkbox',
        message: 'What technologies do you want?',
        choices: [
            {
                name: 'Axios',
                value: 'axios'
            },
            {
                name: 'Eslint',
                value: 'eslint'
            },
            {
                name: 'i18n',
                value: 'i18n'
            },
            {
                name: 'Unit tests',
                value: 'jest'
            },
            {
                name: 'React router',
                value: 'router'
            },
            {
                name: 'Redux',
                value: 'redux'
            },
            {
                name: 'Typescript',
                value: 'ts'
            },
            {
                name: 'Yarn',
                value: 'yarn'
            },
        ]
    },
    {
        name: 'git',
        type: 'confirm',
        message: 'Git init the project ?'
    }
];

const start = async () => {
    const response = await inquirer.prompt(questions);
    console.log(response);

    let additionalDep = '';
    let additionalDevDep = '';
    if (response.technologies.includes('axios')) { additionalDep += ' axios'; }
    if (response.technologies.includes('i18n')) { additionalDep += ' i18next react-i18next'; }
    if (response.technologies.includes('router')) { additionalDep += ' react-router-dom'; }
    if (response.technologies.includes('redux')) { additionalDep += ' redux react-redux'; }

    if (response.technologies.includes('eslint')) { additionalDevDep += ' eslint'; }
    if (response.technologies.includes('jest')) { additionalDevDep += ' jest enzyme enzyme-react-adapter-16'; }
    if (response.technologies.includes('ts')) { additionalDevDep += ' typescript ts-loader'; }

    fs.mkdirSync(__dirname + response.name);
    exec(`cd ${response.name}`);
    if (response.technologies.includes('yarn')) {
        exec(`yarn init -y`);
        exec(`yarn add ${additionalDep}`);
        exec(`yarn add -D ${additionalDevDep}`);
    } else {
        exec(`npm init -y`);
    }
};

start();